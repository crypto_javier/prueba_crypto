var unirest = require("unirest");
const mongoose = require('mongoose');
const Crypto = mongoose.model('Crypto');





exports.callAPi_BraveNewCoin = (req, res, next) => {

    let promesa_callAPi_BraveNewCoin = new Promise((resolve, reject) => {
        console.log("*****");
        var req = unirest("GET", "https://bravenewcoin.p.rapidapi.com/asset");

        req.query({
            "status": "ACTIVE"
        });

        req.headers({
            "x-rapidapi-key": "ec0f229f81mshef36b3dd2e984f2p114956jsn69cfb01ec46a",
            "x-rapidapi-host": "bravenewcoin.p.rapidapi.com",
            "useQueryString": true
        });
        req.end(function (res) {
            if (res.error) throw new Error(res.error);

            //console.log(res.body.content);
            for (moneda of res.body.content) {
                //var crypto = new Crypto();

                var crypto = {}
                crypto.id = moneda.id
                crypto.name = moneda.name
                crypto.symbol = moneda.symbol
                crypto.status = moneda.status
                crypto.type = moneda.type
                crypto.url = moneda.url


                Crypto.updateOne({ "id": crypto.id }, { $set: crypto }, { upsert: true }, (err, doc) => {
                });

            }

            resolve(1);

        });



    });

    promesa_callAPi_BraveNewCoin.then((successMessage) => {
        res.status(200).send({ "ok": "info guardada" });
    });

}

exports.getAllList = (req, res, next) => {


    Crypto.find({}, { id: true, name: true, symbol: true }, (err, cryptos) => {
        res.status(200).send({ "ok": cryptos });
    });
}


exports.setPrice = (idCoin) => {


    var unirest = require("unirest");

    var req = unirest("GET", "https://bravenewcoin.p.rapidapi.com/market-cap");

    req.query({
        "assetId": idCoin
    });

    req.headers({
        "authorization": "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Ik5EVXhNRGhHT0VReE56STVOelJCTTBJM1FrUTVOa0l4TWtRd1FrSTJSalJFTVRaR1F6QTBOZyJ9.eyJpc3MiOiJodHRwczovL2F1dGguYnJhdmVuZXdjb2luLmNvbS8iLCJzdWIiOiJvQ2RRb1pvSTk2RVJFOUhZM3NRN0ptYkFDZkJmNTVSWUBjbGllbnRzIiwiYXVkIjoiaHR0cHM6Ly9hcGkuYnJhdmVuZXdjb2luLmNvbSIsImlhdCI6MTYxMzk5NTI3MSwiZXhwIjoxNjE0MDgxNjcxLCJhenAiOiJvQ2RRb1pvSTk2RVJFOUhZM3NRN0ptYkFDZkJmNTVSWSIsInNjb3BlIjoicmVhZDppbmRleC10aWNrZXIgcmVhZDpyYW5raW5nIHJlYWQ6bXdhIHJlYWQ6Z3dhIHJlYWQ6YWdncmVnYXRlcyByZWFkOm1hcmtldCByZWFkOmFzc2V0IHJlYWQ6b2hsY3YgcmVhZDptYXJrZXQtY2FwIiwiZ3R5IjoiY2xpZW50LWNyZWRlbnRpYWxzIn0.iO-5mBDTxAuZO883mGo6aYIbiWpAlRya0b5h90kCt6eyJLyjyYKEqcO9MpIOX5zFx7DnD4csjSca0-Pl1IoCD3D42b3iJJ0h1TV-JgA189Q4_AHVQWJlZsVm6AgpiG0DVSR6hmGu10xkIHuizcU1d6lalAtEGh3wYn2A1t7bS3RTNOWiofCe8A3ipDPXWHJNox-Dl1DElbwtoHY9oNUFnCx7HEMIQTELRal2Tv2lJ9mf1gtZtp6xWpvkw8TyDi1FFG8UY2zCbLPSKj9PPW810rkI95Jv7o1kLbHr8VCjctjSLFpWVxIhgvJLqE7DePxVsDjq6jiYjpn7FipvVh8uXA",
        "x-rapidapi-key": "ec0f229f81mshef36b3dd2e984f2p114956jsn69cfb01ec46a",
        "x-rapidapi-host": "bravenewcoin.p.rapidapi.com",
        "useQueryString": true
    });


    req.end(function (res) {
        if (res.error) throw new Error(res.error);

        Crypto.updateOne({ "id": idCoin }, { "precio": res.body.content[0].price }, { upsert: true }, (err, doc) => {

        });




        console.log(res.body.content[0].price, idCoin);
    });




}