var unirest = require("unirest");
const mongoose = require('mongoose');
const Crypto = mongoose.model('Crypto');
const User = mongoose.model('User');
const ctrlCrypto = require('./crypto.controller');
const ObjectId = mongoose.Types.ObjectId;



exports.user_addMoneda = (req, res, next) => {
   // console.log(req._id)
    User.updateOne({ "_id": req._id }, { "$addToSet": { "favoritos": req.body.symbol } }, { upsert: true }, (err, doc) => {

        if (!!err)
            res.status(302).send({ "err": "error al agregar" });  //id
        else {
            res.status(200).send({ "ok": "agregada Correctamente" });
            ctrlCrypto.setPrice(req.body.id)
        }
    });
}


exports.user_ListarCrypto = (req, res, next) => {


    query = { "_id": ObjectId(req._id) }

    User.aggregate([
        { $match: query },

        {
            $lookup: {
                from: 'cryptos',
                localField: 'favoritos',
                foreignField: 'symbol',
                as: 'cambio'
            }
        },
        {
            $project: {
                _id: 0,
                "cambio.name": 1,
                "cambio.precio": 1,
                "cambio.symbol": 1,
                
            }
        }




    ], function (err, response) {
       // console.log(response, err)
        return res.status(200).json({ data: response[0].cambio, message: 'ok' });
    });







}



exports.user_top3 = (req, res, next) => {
    query = { "_id": ObjectId(req._id) }

    User.aggregate([
        { $match: query },

        {
            $lookup: {
                from: 'cryptos',
                localField: 'favoritos',
                foreignField: 'symbol',
                as: 'cambio'
            }
        },
        {
            $project: {
                _id: 0,
                "cambio.name": 1,
                "cambio.precio": 1,
                "cambio.symbol": 1,
                "monedaPreferida":1
            }
        }




    ], function (err, response) {
        // response[0].monedaPreferida
        const valorPreferencial = response[0].cambio.filter(crypto => crypto.symbol == response[0].monedaPreferida);

        var top = response[0].cambio.map(function(x) {
            x.valFavorito=x.precio/valorPreferencial[0].precio  
            return x ;
         });

         ordenado=top.sort(function(a,b){return b.valFavorito - a.valFavorito}) 
        

        return res.status(200).json({ data: ordenado.slice(1, 4) , message: 'ok' });
    });

     



}
 