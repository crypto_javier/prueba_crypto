const mongoose = require('mongoose');
const passport = require('passport');

const User = mongoose.model('User');
const Crypto = mongoose.model('Crypto');

const async = require('async');



exports.crearUsuario = (req, res, next) => {
    var user = new User();

    
    user.Nombre = req.body.Nombre
    user.Apellido = req.body.Apellido
    user.UserPassword = req.body.UserPassword
    user.Username = req.body.Username
    user.monedaPreferida = req.body.monedaPreferida.toUpperCase() 

    //console.log(user);


    User.findOne({ Username: req.body.Username }, // verificar  si el usuario existe
        (err, user1) => {

            if (err) {
                res.status(302).send({ "err": "error" });
            } else if (user1) {
                res.status(302).send({ "err": "usuario duplicado" });
            } else {

                Crypto.findOne({ symbol:  user.monedaPreferida },{symbol:true}, (err, moneda) => {  // verificar si la moneda existe
                    if (!!moneda) {
                        user.save((err, doc) => {       // crear el usuario
                 
                            if (!err) {
                                res.status(200).send({ "ok": "ok" });
                            } else {
                                res.status(302).send({ "err": "error" });
                            }
                        });
                    } else {
                        res.status(302).send({ "err": "La moneda no existe" });
                    }

                });
            }
        });

}

 
module.exports.authenticate = (req, res, next) => {

 
    passport.authenticate('local', (err, user, info) => {
         
        // error from passport middleware
        if (err) {
            // return res.status(200).json({ status: false, message: 'ml' });
            return res.status(302).json(err);
        }
        // registered user
        else if (user) {
            return res.status(200).json({ "token": user.generateJwt(),"Username":user.Username,"id":user._id});

        }
        // unknown user or wrong password
        else {
            return res.status(302).json(info);
        }
    })(req, res);


}