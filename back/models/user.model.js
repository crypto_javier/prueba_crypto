const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');


var userSchema = new mongoose.Schema({

    Nombre: {
        type: String,
        required: 'no puede ser Vacío  ',
    },
    

    Apellido: {
        type: String,
        required: 'no puede ser Vacío  ',
    },
    UserPassword: {
        type: String,
        required: 'no puede ser Vacío o debe ser alfánumerica ',
        minlength: [8, 'el Password debe contener mas de 8 caracteres']
    },
    
    Username : {
        type: String,
        required: 'Username no puede ser Vacío',
        unique: true
    },
 
    monedaPreferida: {
        type: String,
        required: 'no puede ser Vacío  ',
    },
    favoritos:{
        type: Array,
    }
 
     



});
// Custom validation for Username
userSchema.path('UserPassword').validate((val) => {
    UserPasswordRegex = /^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$/;
    return UserPasswordRegex.test(val);
}, 'UserPassword invalido debe contener numeros y mayusculas');

 


// Events
userSchema.pre('save', function (next) {
    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(this.UserPassword, salt, (err, hash) => {
            this.UserPassword = hash;
            this.saltSecret = salt;
            next();
        });
    });
});

userSchema.pre('claveHash', function (next) {
    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(this.UserPassword, salt, (err, hash) => {
            this.UserPassword = hash;
            this.saltSecret = salt;

        });
    });
});

// Methods
userSchema.methods.verifyPassword = function (UserPassword) {
    return bcrypt.compareSync(UserPassword, this.UserPassword);
};

userSchema.methods.generateJwt = function () {
    return jwt.sign({ _id: this._id },
        process.env.JWT_SECRET,
        {
            expiresIn: process.env.JWT_EXP
        });
}


mongoose.model('User', userSchema);