const mongoose = require('mongoose');


const cryptoSchema = new mongoose.Schema({
   
    id: {
        type: String,
        required: true,
        
    },
    name: {
        type: String,
        required: true
    },
    symbol: {
        type: String,
        required: true
    },
    status: {
        type: String,
        required: true
    },
    type: {
        type: String,
        required: true
    },
    url: {
        type: String,
        
    },
    precio: {
        type: Number,
       
    } 


});

 mongoose.model('Crypto', cryptoSchema);

