const mongoose = require('mongoose');

const config = {
    autoIndex: false,
    useNewUrlParser: true,
  };

mongoose.connect(process.env.MONGODB_URI, config ,(err) => {
    if (!err) {
        console.log('MongoDB connection succeeded.'); 
    }
    else { 
        console.log('Error in MongoDB connection : ' + JSON.stringify(err, undefined, 2)); 
    }
});



require('./user.model');
require('./crypto.model');
 