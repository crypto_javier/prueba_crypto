require('./config/config');
require('./models/db');
require('./config/passportConfig');


//---------------------
const http = require('http');
const express = require('express');
var app = express();

const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const rtsIndex = require('./routes/index.router');
const passport = require('passport');
const cors = require('cors');



//---------------------------------
// middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
app.use('/', rtsIndex);


 
const PORT = 2122; 
const hostname = '127.0.0.1';
const server = http.createServer(app); 
server.listen(PORT, () => {
    console.log(`Server running at http://${hostname}:${PORT}/`);

});
