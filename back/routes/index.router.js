const express = require('express');
const router = express.Router();
const ctrlUser = require('../controllers/user.controller');
const ctrlCrypto = require('../controllers/crypto.controller');
const ctrlCryptoUser = require('../controllers/crypto_user.controller');
//-------------------------------------------------
const jwtHelper = require('../config/jwtHelper');
 
//-----------------------uaurario-
router.post('/registro/crearUsuario', ctrlUser.crearUsuario);;

router.post('/authenticate', ctrlUser.authenticate);
//-------------
router.get('/crypto/callAPi_BraveNewCoin', ctrlCrypto.callAPi_BraveNewCoin);;
router.get('/crypto/getAllList', ctrlCrypto.getAllList);;
//-------------

 router.post('/crypto_user/user_addMoneda',jwtHelper.verifyJwtToken, ctrlCryptoUser.user_addMoneda );  
 router.get('/crypto_user/user_ListarCrypto',jwtHelper.verifyJwtToken, ctrlCryptoUser.user_ListarCrypto  ); //,jwtHelper.verifyJwtToken
 router.get('/crypto_user/user_top3',jwtHelper.verifyJwtToken, ctrlCryptoUser.user_top3  ); //,jwtHelper.verifyJwtToken

  
 
//--------
router.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', false);

    // Pass to next layer of middleware
    next();
});


module.exports = router;