// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiBaseUrl: 'http://localhost:2122',
  keyRecap:'6LcnePEUAAAAAOC5n_r3yZbMbkixQ4xXMBwd3XN1'
};
/*



export const environment = {
  production: false,
  apiBaseUrl: 'http://localhost:8080',  https://marginp2p.rj.r.appspot.com
  keyRecap:'6Lc0P9EUAAAAAMVzJ1Yk-g1LZ7Y0LBQA4Oj2MsnU'
};






 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
