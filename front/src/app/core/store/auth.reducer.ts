import {on , createReducer, Action }  from '@ngrx/store'; 
import * as auth from './auth.actions'

export interface loginState{   //interface
    isAuthenticated: boolean,
  
}

export const initialState: loginState = {
    isAuthenticated: false,
    
  };


const reducer = createReducer(
    initialState,
    on(auth.authLogin, state => ({ ...state, isAuthenticated: true })),
    on(auth.authLogout, state => ({ ...state, isAuthenticated: false }))
)

export function authReducer(
    state: loginState | undefined,
    action: Action
  ) {
    return reducer(state, action);
  }
  