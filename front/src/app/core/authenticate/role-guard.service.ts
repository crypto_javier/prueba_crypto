import { UserService } from "../shared/user.service";
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, Route } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RoleGuardService {

  constructor(private userService : UserService,private router : Router){}


  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
     
       if (this.userService.selectedUser.idGrupo == next.data.rol) {
         
        return false;
      }
      return true;
      

 
  }

}
