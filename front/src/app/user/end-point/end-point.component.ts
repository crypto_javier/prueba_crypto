import { Component, OnInit } from '@angular/core';
import { UserService } from '../shared/user.service';

@Component({
  selector: 'app-end-point',
  templateUrl: './end-point.component.html',
  styleUrls: ['./end-point.component.scss']
})
export class EndPointComponent implements OnInit {

  constructor(
    public userService:UserService,

  ) { }


  retultado = "";

  ngOnInit(): void {
  }


  lista() {

    this.userService.ListarCrypto().subscribe(

      res => {
        let result: any = res
        this.retultado=result.data
        console.log(result.data)

     
      },
      err => { console.log(err) });


  }
  top() {

    this.userService.top3().subscribe(

      res => {
        let result: any = res
        this.retultado=result.data
        console.log(result.data)


     
      },
      err => { console.log(err) });



  }

}
