import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CrearListaComponent } from './crear-lista/crear-lista.component';
import { EndPointComponent } from './end-point/end-point.component';
import { UserComponent } from './user.component';
import { AuthGuard } from '../core/authenticate/auth.guard';

const routes: Routes = [

  
  {
    
    path:  '', component:EndPointComponent ,
    children: [

      { path: 'crear', component:CrearListaComponent   },
      { path: 'endPoint', component:EndPointComponent    },
    ]
  
  },



];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
