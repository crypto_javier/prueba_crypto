import { Component, Inject, OnInit, ElementRef, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { UserService } from '../shared/user.service';

import { MatTableDataSource } from '@angular/material/table';
import { MatSortModule, MatSort } from '@angular/material/sort';
import {MatSnackBar} from '@angular/material/snack-bar';

 

@Component({
  selector: 'app-crear-lista',
  templateUrl: './crear-lista.component.html',
  styleUrls: ['./crear-lista.component.scss']
})
export class CrearListaComponent implements OnInit {
  displayedColumns: string[] = ['name','symbol','accion' ];
  dataSource = new MatTableDataSource(ELEMENT_DATA)
  isLoad=true
  idUser=""

  @ViewChild(MatSort, { static: false }) sort !: MatSort;
 


  constructor(
    public userService:UserService,
    public snackBar: MatSnackBar,

  ) { }

  ngOnInit(): void {

     
    this.idUser=sessionStorage.getItem("id") || ""

    this.userService.getAllList().subscribe(
      res => {
        let result: any = res
        const ELEMENT_DATA: dataElement[] =result["ok"]
        this.dataSource = new MatTableDataSource(ELEMENT_DATA)
       // console.log(result)
        this.isLoad=false
     
      },
      err => { console.log(1) });
  }

  
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


  add(element:any){

    let data={
      symbol:element.symbol,
      id:element.id,
   
    }

    
    this.userService.addMoneda(data).subscribe(

      res => {
        let result: any = res
       
       

        this.snackBar.open("Agregado "+element.symbol, "", {
          duration: 3000,
          panelClass: ['err-class'] 
        });
       
     
      },
      err => { console.log(err) });

  }

}


export interface dataElement {
  _id: string;
  name: string;
  symbol: string;
 
}

const ELEMENT_DATA: dataElement[] = [
];
