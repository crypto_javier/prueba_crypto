import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';


 
@Injectable({
  providedIn: 'root'
})
export class UserService {
  
  constructor(private http: HttpClient) { }
  
  
  getAllList() {
 
    return this.http.get(environment.apiBaseUrl + '/crypto/getAllList' );
  }

 

  addMoneda(value: any) {
 
    return this.http.post(environment.apiBaseUrl + '/crypto_user/user_addMoneda',value );
  }

  
  ListarCrypto() {
    return this.http.get(environment.apiBaseUrl + '/crypto_user/user_ListarCrypto' );
  }

  top3() {
    return this.http.get(environment.apiBaseUrl + '/crypto_user/user_top3' );
  }

 
 
}
