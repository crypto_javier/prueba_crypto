import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './core/authenticate/auth.guard';

const routes: Routes = [


  { path: 'user', loadChildren: () => import('./user/user.module').then(m => m.UserModule),canActivate:[AuthGuard] },
  { path: '', loadChildren: () => import('./home/home.module').then(m => m.HomeModule),pathMatch: 'full' },
  { path: '**', loadChildren: () => import('./home/home.module').then(m => m.HomeModule),pathMatch: 'full' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
