import { ChangeDetectorRef, Component, OnDestroy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { NavItem } from "./core/menu/nav-item";
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { UserService } from './core/shared/user.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import * as actionAuth from './core/store/auth.actions'


import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';


import { SelectionModel } from "@angular/cdk/collections";
import { MediaMatcher } from "@angular/cdk/layout";



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  selectedLenguage = 'es';
  raiz = ""
  validateForm!: FormGroup;
  isLogin = false
  auth$!: Observable<boolean>;

  mobileQuery: MediaQueryList;
  fillerNav = Array.from({ length: 50 }, (_, i) => `Nav Item ${i + 1}`);
  private _mobileQueryListener: () => void;
  shouldRun = true

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );


  miMenu: NavItem[] = []


  constructor(
    public translate: TranslateService,
    public dialog: MatDialog,
    private store: Store<{  auth: any }>,
    changeDetectorRef: ChangeDetectorRef, media: MediaMatcher,
    private breakpointObserver: BreakpointObserver,
    public userService: UserService,
    private router: Router,
    private fb: FormBuilder,

  ) {
    //this.translate.addLangs(["es", "en"]);
    this.translate.setDefaultLang("es");

    this.mobileQuery = media.matchMedia("(max-width: 700px)");
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  bandeda=0
  ngOnInit(): void {

    this.validateForm = this.fb.group({
      Username: ["casas3", [Validators.required, Validators.email]],
      Password: ["Asredtr32", [Validators.required, Validators.minLength(8)]]
    });


    this.auth$= this.store.pipe(select('auth'))
    this.isLogin = this.userService.isLoggedIn()

    

    if (this.isLogin) {
      this.router.navigateByUrl('/' + 'user');
    }


    this.auth$.subscribe(isLogin => {
      if(this.bandeda==1){
        this.isLogin = Object.values(isLogin)[0]
      }
     });

    this.bandeda=1

  }


  ingresar() {


    this.userService.login(this.validateForm.value).subscribe(
      res => {

        let result: any = res
        console.log(result)

        if (!result['token']) {
          console.log("error")
        } else {
          
          this.userService.selectedUser.idGrupo = result['token2'];
          this.userService.setToken(result['token']);
          this.store.dispatch(actionAuth.authLogin())

          sessionStorage.setItem('user', result['Username']);
          sessionStorage.setItem('id', result['id']);
          this.router.navigateByUrl('/' + 'user');

        }





      },
      err => {
        console.log(err)
        //lertas.home.verificaCredencial

      });





  }


  onLogout() {
    this.store.dispatch(actionAuth.authLogout())
    sessionStorage.clear();
    this.userService.deleteToken();
    this.isLogin = false;
    this.router.navigateByUrl('/');
    this.raiz=""
     
    //window.location.reload();
  }



}
