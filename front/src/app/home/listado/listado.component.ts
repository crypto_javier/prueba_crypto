import { Component, OnInit ,ChangeDetectionStrategy,ViewChild,HostBinding, ElementRef, HostListener} from '@angular/core';
import { ApiService } from '../shared/api.service';
import axios from "axios";
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';


import {fromEvent} from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';
 
 

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.scss'],
  
})
export class ListadoComponent implements OnInit {

  
  @ViewChild('div', {static: true}) _div!: ElementRef;


  displayedColumns: string[] = ['name','Precio','type','id' ];  
   
  isLoad=true
  allData=[]
  scrollDistance = 1;
  scrollUpDistance = 2;
  limF=20;
  bandera=true

   
  dataSource = new MatTableDataSource(ELEMENT_DATA)
 
  constructor(
    public apiService: ApiService,
    
             
  ) { }

 




  ngOnInit(): void {

    fromEvent(this._div.nativeElement,'scroll').subscribe((e: any) => {
       
      let scrollHeight=e.target['scrollHeight']-e.target['clientHeight']




       if(scrollHeight < e.target['scrollTop'] && this.bandera){
        this.bandera=false;
        this.limF=this.limF+20;
        const ELEMENT_DATA: dataElement[] =this.allData.slice(0,this.limF)
        this.dataSource = new MatTableDataSource(ELEMENT_DATA)
   
      }else{
        this.bandera=true
      }
     
       

      
    });
    


    const options = {
      url: 'https://bravenewcoin.p.rapidapi.com/asset',

      headers: {
        'x-rapidapi-key': 'ec0f229f81mshef36b3dd2e984f2p114956jsn69cfb01ec46a',
        'x-rapidapi-host': 'bravenewcoin.p.rapidapi.com'
      }
    };


    axios.request(options).then( (response) => {
      this.isLoad=false
      console.log(response.data);
      this.allData=response.data.content
      const ELEMENT_DATA: dataElement[] =response.data.content.slice(0,this.limF)
      this.dataSource = new MatTableDataSource(ELEMENT_DATA)
      
    }).catch(function (_error) {
      //console.error(error);
       
    });
  }

  cambiar(_elemento:any){

  }


  
   


}


export interface dataElement {
  name: string;
  Precio: string;
  type: string;
  id: string;
 
}

const ELEMENT_DATA: dataElement[] = [
];

