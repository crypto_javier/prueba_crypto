import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {MatTabsModule} from '@angular/material/tabs';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {MatTableModule} from '@angular/material/table';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatDialogModule} from '@angular/material/dialog';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatSortModule} from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatMenuModule} from '@angular/material/menu';
 


import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from "@angular/flex-layout";
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

import { HomeRoutingModule } from './home-routing.module';
import { InicioComponent } from './inicio/inicio.component';
import { CambioComponent } from './cambio/cambio.component';
import { ListadoComponent } from './listado/listado.component';
import { HomeComponent } from './home.component';
import {ScrollingModule} from '@angular/cdk/scrolling';




@NgModule({
  declarations: [InicioComponent, CambioComponent, ListadoComponent, HomeComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,

    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    InfiniteScrollModule,
    
    MatTabsModule,
    MatButtonModule,
    MatIconModule,
    MatFormFieldModule,
    MatCardModule,
    MatInputModule,
    MatTableModule,
    MatProgressBarModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatPaginatorModule,
    MatSnackBarModule,
    MatMenuModule,
    MatTabsModule,
    ScrollingModule
  ]
})
export class HomeModule { }
